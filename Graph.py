#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pprint import pprint
import numpy as np # for Floyd-Warshall matrices
import queue
import copy

################################
# Graph manipulation functions #
################################
def create_graph(directed = True, weighted = False): # TP1
    """
    Create a dictionnary representing a graph and return it.
    """
    g = {'nodes': {}, 
         'edges': {},
         'reverse_edges' : {},
         'nb_edges': 0, 
         'directed': directed, 
         'weighted': weighted, 
         'weight_attribute': None
        }
    return g

def add_node(g, n, attributes = None): # TP1
    """
    Add a node n (node id provided as a string or int) to the graph g.
    Attributes on the node can be provided by a dictionnary.
    Returns the node n attributes.
    """
    if n not in g['nodes']: # ensure node does not already exist
        if attributes is None: # create empty attributes if not provided
            attributes = {}
        g['nodes'][n] = attributes
        g['edges'][n] = {} # init outgoing edges
        if g['directed']:
            # init ingoing edges if g is directed
            g['reverse_edges'][n] = {}
            
    return g['nodes'][n] # return node attributes

def add_edge(g, n1, n2, attributes = None, n1_attributes = None, n2_attributes = None): # TP1
    """
    Add an edge from n1 to n2.
    Create the nodes if they don't exist.
    If g is directed, add the reversed edge to reverse_edges.
    If g is undirected, add the reversed edge to edges.
    """
    # create nodes if they do not exist
    if n1 not in g['nodes']: add_node(g, n1, n1_attributes) # ensure n1 exists
    if n2 not in g['nodes']: add_node(g, n2, n2_attributes) # ensure n2 exists
    # add edge(s) only if they do not exist
    if n2 not in g['edges'][n1]:
        if attributes is None: # create empty attributes if not provided
            attributes = {}
        g['edges'][n1][n2] = attributes
        if not g['directed']:
            g['edges'][n2][n1] = g['edges'][n1][n2] # share the same attributes as n1->n2        
        else :
            # add reverse edge if g is directed
            g['reverse_edges'][n2][n1] = g['edges'][n1][n2] # share the same attributes as n1->n2
            
        g['nb_edges'] += 1
        
    return g['edges'][n1][n2] # return edge attributes

def load_SIF(filename, directed=True): # TP1
    """
    parse a SIF (cytoscape Simple Interaction Format) file and returns a directed graph.
    line syntax: nodeD <relationship type> nodeE nodeF nodeB
    """
    g = create_graph(directed) # new empty graph
    with open(filename) as f: # OPEN FILE
        # PROCESS THE REMAINING LINES
        row = f.readline().rstrip() # read next line and remove ending whitespaces
        while row:
            vals = row.split('\t') # split line on tab
            for i in range(2, len(vals)):
                att = { 'type': vals[1] } # set edge type
                add_edge(g, vals[0], vals[i], att)
            row = f.readline().rstrip() # read next line
    return g # return created graph

def load_TAB(filename, directed=True, weighted=False, weight_attribute=None): # TP3
    """
    parse a TAB file (as cytoscape format) and returns a graph.
    
    line syntax: id1    id2    att1    att2    att3    ...
    """
    g = create_graph(directed, weighted)
    g['weight_attribute'] = weight_attribute
    with open(filename) as f: 
        # GET COLUMNS NAMES
        tmp = f.readline().rstrip()
        attNames= tmp.split('\t')
        # REMOVES FIRST TWO COLUMNS WHICH CORRESPONDS TO THE LABELS OF THE CONNECTED VERTICES
        attNames.pop(0)
        attNames.pop(0)
        # PROCESS THE REMAINING LINES
        row = f.readline().rstrip()
        while row:
            vals = row.split('\t')
            u = vals.pop(0)
            v = vals.pop(0)
            att = {}
            for i in range(len(attNames)):
                att[ attNames[i] ] = vals[i]
            add_edge(g, u, v, att)
            row = f.readline().rstrip() # NEXT LINE
    return g

def test_graph_manipulation():
    newtest()
    print("Test des fonction de manipulation de graphes sur M1BBS_Graphe_dressing.sif :\n")
    print("Chargement du graphe :")
    pprint(load_SIF('M1BBS_Graphe_dressing.sif'))

################################
######## Basic getters #########
################################

def get_nodes(g):
    """ List of the nodes of g"""
    return list(g['nodes'].keys())

def get_succ(g,v):
    """ List of the successors of v in g"""
    return list(g['edges'][v].keys())

def get_pred(g,v):
    """ List of the predecessors of v in g"""
    if g['directed']:
        return list(g['reverse_edges'][v].keys())
    else: # Same definition as successors if undirected
        get_succ(g,v)

################################
############# BFS ##############
################################

def BFS(G,s, invert=False):
    """BFS on the vertices accessibles from s
    If invert is true, execute the BFS on the transposed graph instead.
    """    
    color = {v : 'WHITE' for v in get_nodes(G)}
    d = {v : float('inf') for v in get_nodes(G)}
    pred = {v : None for v in get_nodes(G)}
    
    color[s] = 'GRAY'
    d[s] = 0
    
    v_queue = queue.Queue()
    v_queue.put(s)
    
    while not v_queue.empty():
        u = v_queue.get()
        if not invert :
            succ = get_succ(G,u)
        else :
            succ = get_pred(G,u)
        for v in succ:
            if color[v] == 'WHITE' :
                color[v] = 'GRAY'
                d[v] = d[u] +1
                pred[v] = u
                v_queue.put(v)
        color[u] = 'BLACK'
    
    return { 'color' : color, 'd' : d, 'pred' : pred}

def test_BFS():
    newtest()
    print("Test de BFS sur M1BBS_Graphe_dressing.sif :\n")
    
    g = load_SIF('M1BBS_Graphe_dressing.sif')
    res = BFS(g,'sous-vetements')
    
    print("BFS depuis sous-vêtements :")
    pprint(res)

################################
##### DFS single component #####
################################

# Just because it's easy to implement from the BFS algorithm
    
def DFS_sc(G,s):
    """DFS on the vertices accessibles from s"""    
    color = {v : 'WHITE' for v in get_nodes(G)}
    d = {v : float('inf') for v in get_nodes(G)}
    pred = {v : None for v in get_nodes(G)}
    
    color[s] = 'GRAY'
    d[s] = 0
    
    v_queue = queue.LifoQueue()
    v_queue.put(s)
    
    while not v_queue.empty():
        u = v_queue.get()
        for v in get_succ(G,u):
            if color[v] == 'WHITE' :
                color[v] = 'GRAY'
                d[v] = d[u] +1
                pred[v] = u
                v_queue.put(v)
        color[u] = 'BLACK'
    
    return { 'color' : color, 'd' : d, 'pred' : pred}

def test_DFS_sc():
    newtest()
    print("Test de DFS détectant une seule composante connexe",
          "sur M1BBS_Graphe_dressing.sif :\n")
    g = load_SIF('M1BBS_Graphe_dressing.sif')
    res = DFS_sc(g,'sous-vetements')
    print("DFS mono-composante depuis sous-vêtements :\n")
    pprint(res)

################################
######### Complete DFS #########
################################
    
def DFS(G):
    """DFS on the vertices accessibles from s"""    
    # Note : using time as a list is not what I'd call clean,
    # But it's needed to avoid passing it as an argument to DFS_visit
    # - python scopes are clunky -
    time = [0]
    color = {v : 'WHITE' for v in get_nodes(G)}
    d = {v : float('inf') for v in get_nodes(G)}
    f = {v : float('inf') for v in get_nodes(G)}
    pred = {v : None for v in get_nodes(G)}
    edge_class = {}
        
    def DFS_visit(u) :
        color[u] = 'GRAY'
        time[0] += 1
        d[u] = time[0]
        for v in get_succ(G,u):
            if color[v] == 'WHITE':
                pred[v] = u
                DFS_visit(v)
                edge_class[(u,v)] = 'TREE_EDGE'
            elif color[v] == 'GRAY':
                edge_class[(u,v)] = 'BACK_EDGE'
            elif d[u] > d[v]:
                edge_class[(u,v)] = 'CROSS_EDGE'
            else :
                edge_class[(u,v)] = 'FORWARD_EDGE'
        
        color[u] = 'BLACK'
        time[0] += 1
        f[u] = time[0]
    
    for u in get_nodes(G):
        if color[u] == 'WHITE':
            DFS_visit(u)

    return { 'color' : color, 'd' : d, 'f' : f, 'pred' : pred, 'edge_class' : edge_class}

def test_DFS():
    newtest()
    print("Test de DFS complet sur M1BBS_Graphe_dressing.sif :\n")
    g = load_SIF('M1BBS_Graphe_dressing.sif')
    res = DFS(g)
    print("DFS complet :")
    pprint(res)

def is_acyclic(G):
    return not('BACK_EDGE' in DFS(G)['edge_class'])

def test_is_acyclic():
    newtest()
    print("test de is_acyclic sur M1BBS_Graphe_dressing.sif :\n")
    print(is_acyclic(load_SIF('M1BBS_Graphe_dressing.sif')))

def topological_sort(G):
    """ 
    Return the vertices of G sorted by a topological sort.
    G is supposed acyclic.
    """
    dico = DFS(G)['f']
    return [(k,dico[k]) for k in sorted(dico,key = dico.get, reverse=True)]

def test_topological_sort():
    newtest()
    print("test du tri topologique sur M1BBS_Graphe_dressing.sif :\n")
    print(topological_sort(load_SIF('M1BBS_Graphe_dressing.sif')))

################################
######### Bellman-ford #########
################################
    
def bellmanFord(G,s,w):
    """
    Bellman Ford (1 to All) on G from s on the attribute w.
    Returns a dictionnary where 'd' contains the distances and 'pi' the paths.
    """
    
    #initialise_single_source(s)
    vertices = get_nodes(G)
    d = {v : float('inf') for v in vertices}
    pi = {v : None for v in vertices}
    d[s] = 0

    
    for i in range(1,len(vertices)):
        for (u,succ) in G['edges'].items():
            for (v,attr) in succ.items():
                #relax(u,v)
                if d[v] > d[u] + float(attr[w]):
                    d[v] = d[u] + float(attr[w])
                    pi[v] = u

    return {'d':d, 'pi':pi}        

def test_BellmanFord():
    newtest()
    print("Test de Bellman-Ford sur M1BBS_Graphe_Bellman-Ford.tab :\n")
    g = load_TAB("M1BBS_Graphe_Bellman-Ford.tab", weighted= True, weight_attribute= "weight")
    print("Résultats de Bellman-ford à partir du sommet A :")
    pprint(bellmanFord(g,'A',g['weight_attribute']))

################################
#### Bonus : Floyd-Warshall ####
################################
    
def adjacency_matrix(G,w):
    """ Returns the adjagency matrix of G (numpy array)
    and a dictionnary of vertices associated to their position in the matrix.
    """
    vertices = {v : i for i,v in enumerate(get_nodes(G))}
    #vertices = sorted(get_nodes(G))
    m = np.full((len(vertices),len(vertices)),np.inf,dtype=float)
    
    for (u,succ) in G['edges'].items():
            for (v,attr) in succ.items():
                m[vertices[u],vertices[v]] = attr[w]
    
    return m,vertices
    
def floydWarshall(G):
    """ Floyd-Warshall (All To All) on G.
    Returns the weights, distances and next vertex matrices
    and a dictionnary of vertices associated to their position in the matrix.
    """
    W,ids = adjacency_matrix(G,G['weight_attribute'])
    D = np.copy(W)
    
    N = np.full((len(ids),len(ids)),None,dtype=str)
    for (u,succ) in G['edges'].items():
        for (v,_) in succ.items():
            N[ids[u]][ids[v]] = v
    
    for k in range(len(ids)):
        for i in range(len(ids)):
            for j in range(len(ids)):
                if D[i,k] + D[k,j] < D[i,j]:
                    D[i,j] = D[i,k] + D[k,j]
                    N[i,j] = N[i,k]

    return (W,D,N,ids)

def floydWarshallPath(D,N,source, destination,ids):
    """ Returns the best path from 
    source to path as determined by Floyd-Warshall if it exists.
    """
    if D[ids[source],ids[destination]] == np.inf:
        return None
    else :
        path = [source]
        k = N[ids[source],ids[destination]]
        while k != destination:
            path.append(k)
            k = N[ids[k],ids[destination]]
        path.append(destination)
    return path

#    #    #    #    #    #    #    #    #    #    #    #    #    #    #
# Bonus Bis : Diameter, using Floyd-Warshall, so in the same section. #
#    #    #    #    #    #    #    #    #    #    #    #    #    #    #
    
def diameter(G):
    """ Returns the diameter of the graph G """
    # Get an all to all distance matrix
    _,D,_,_ = floydWarshall(G)
    # Replace infinity by nan, while flattening D
    Dbis = list(map(lambda n: np.nan if n == np.inf else n, 
                             D.flatten()))
    # Compute the longest shortest path
    return np.nanmax(Dbis)

def test_FloydWarshall():
    newtest()
    print("Test de Floyd-Warshall sur M1BBS_Graphe_Floyd-Warshall.tab :\n")
    
    g = load_TAB("M1BBS_Graphe_Floyd-Warshall.tab", weighted= True, weight_attribute= "weight")
    W,D,N,ids = floydWarshall(g)
    
    print("Matrice d'adjacence : \n",W,"\n")
    print("Matrice de distances : \n",D,"\n")
    print("Matrice de successeur pour le meilleur chemin : \n",N,"\n")
    
    print("Meilleur chemin de A à C : \n",floydWarshallPath(D,N,"A","C",ids),"\n")
    print("Diamètre du graphe : ", diameter(g),"\n")


################################
######### Main / Tests #########
################################

def newtest():
    print("\n========================================\n")

if __name__ == "__main__":
    newtest()
    print("Graph lib tests")
    test_graph_manipulation()
    test_BFS()
    test_DFS()
    test_is_acyclic()
    test_topological_sort()
    test_BellmanFord()
    test_FloydWarshall()
    # ~ test_Dijkstra()
    # ~ test_Johnson()
