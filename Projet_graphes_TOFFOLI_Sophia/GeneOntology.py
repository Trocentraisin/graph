#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pprint import pprint
import Graph as gr # Graph library from part 1 of the project
import re
import numpy as np
import time
import copy

def load_OBO(filename):
    """
    parse the OBO file and returns the graph
    obsolete terms are discarded
    only id, name, namespace, def, is_a and relationship tags are loaded

    Extract of a file to be parsed:
    [Term]
    id: GO:0000028
    name: ribosomal small subunit assembly
    namespace: biological_process
    def: "The aggregation, arrangement and bonding together of constituent RNAs and proteins to form the small ribosomal subunit." [GOC:jl]
    subset: gosubset_prok
    synonym: "30S ribosomal subunit assembly" NARROW [GOC:mah]
    synonym: "40S ribosomal subunit assembly" NARROW [GOC:mah]
    is_a: GO:0022618 ! ribonucleoprotein complex assembly
    relationship: part_of GO:0042255 ! ribosome assembly
    relationship: part_of GO:0042274 ! ribosomal small subunit biogenesis
    """
    def parseTerm(lines):
        # search for obsolete
        for l in lines:
            if l.startswith('is_obsolete: true'): return
        # otherwise create node
        lines.pop(0) # [Term] line
        id = lines.pop(0)[4:].rstrip()
        term = gr.add_node(g,id)
        term['id'] = id
        term['type'] = 'GOTerm'
        for line in lines:
            # attributes (name, namespace, def)
            if line.startswith('name: '): term['name'] = line[6:]
            elif line.startswith('namespace: '): term['namespace'] = line[11:]
            elif line.startswith('def: '): term['def'] = line[5:]
            elif line.startswith('alt_id: '): g['alt_id'][ line[8:] ] = id # alternate ids
            # relationships
            elif line.startswith('is_a:'): # is_a
                parent = line[6:line.index('!')].rstrip()
                e = gr.add_edge(g, parent, id)
                e['type'] = 'is_a'
            elif line.startswith('relationship:'): # relationship
                relationship_pattern = re.compile(
                        r"^relationship: (?P<type>[^ ]*) (?P<dest>GO:\d*) !.*")
                match_line = re.search(relationship_pattern,line)
                e = gr.add_edge(g, match_line.group('dest'), id)
                e['type'] = match_line.groups('type')
    # instantiate directed graph and additionnal graph attributes
    g=gr.create_graph(directed=True, weighted=False)
    g['alt_id'] = {} # alternate GO ids
    with open(filename) as f: 
        line = f.readline().rstrip()
        # skip header to reach 1st Term
        told = f.tell()
        while not line.startswith('[Term]'): 
            line = f.readline().rstrip()
        buff = []
        while line: # line = [Term]
            while line!="": # buffering lines until empty line
                buff.append(line) # append to buffer
                line = f.readline().rstrip()
            # parse buffer
            parseTerm(buff)
            buff = []
            # find next [Term]
            while not line.startswith('[Term]') and f.tell()!=told: 
                told=f.tell()
                line = f.readline().rstrip()
    return g

def load_GOA(go, filename):
    """
    parse GOA file and add annotated gene products to previsouly loaded graph go

    Extract of a file to be parsed:
    !gaf-version: 2.1
    !GO-version: http://purl.obolibrary.org/obo/go/releases/2016-10-29/go.owl
    UniProtKB  A5A605  ykfM      GO:0006974  PMID:20128927   IMP              P  Uncharacterized protein YkfM    YKFM_ECOLI|ykfM|b4586         protein taxon:83333  20100901  EcoCyc
    UniProtKB  A5A605  ykfM      GO:0016020  GO_REF:0000037  IEA              C  Uncharacterized protein YkfM    YKFM_ECOLI|ykfM|b4586         protein taxon:83333  20161029  UniProt
    UniProtKB  P00448  sodA      GO:0004784  GO_REF:0000003  IEA  EC:1.15.1.1 F  Superoxide dismutase [Mn]       SODM_ECOLI|sodA|JW3879|b3908  protein taxon:83333  20161029  UniProt
    UniProtKB  P00393  ndh  NOT  GO:0005737  PMID:6784762    IDA              C  NADH dehydrogenase              DHNA_ECOLI|ndh|JW1095|b1109   protein taxon:83333  20100621  EcoliWiki
        0        1       2   3       4             5          6        7      8             9                              10
                 id    name        go_id               evidence-codes                     desc                           aliases
    """
    names = {}
    go['names'] = names # gene names or gene product names (column 3)
    with open(filename) as f: 
        line = f.readline()
        while line:
            if not line.startswith('!'):
                cols = line.rstrip().split('\t')
                id = cols[1]
                go_id = cols[4]
                if go_id not in go['nodes']: # GOTerm not found search alternate ids
                    if go_id in go['alt_id']: # success
                        go_id = go['alt_id'][go_id] # replace term
                    else: # warn user
                        print('Warning: could not attach a gene product (%s) to a non existing GO Term (%s)' % (id, go_id))
                if go_id in go['nodes']:
                    # create node for gene product if not already present
                    if id not in go['nodes']:
                        g = gr.add_node(go,id)
                        g['id'] = id
                        g['type'] = 'GeneProduct'
                        names[cols[2]] = id
                    # create or update gene product attributes
                    gp = go['nodes'][id]
                    gp['name'] = cols[2]
                    gp['desc'] = cols[9]
                    gp['aliases'] = cols[10]
                    # attach gene product to GOTerm
                    # go_term = go['nodes'][go_id]
                    e = gr.add_edge(go, go_id, id)
                    e['type'] = 'annotation'
                    if 'evidence-codes' not in e: e['evidence-codes'] = []
                    e['evidence-codes'].append( cols[6] )
                else: # go_id or alt_id not found in GOTerms
                    print('Error: could not attach a gene product (%s) to non existing GO Term (%s)' % (id, go_id))
            line = f.readline()

#####


def max_depth(go, node):
    """
    go graph traversal to find the longest path length from node (GOTerm) to a leaf (node with no successor)
    
    Returns the length of the longest path.
    
    Supposes go is acyclic
    """
    
    memo = {}
    
    def max_depth_aux(node):
        """
        auxilliar reccursive method to make the recursive call easier
        """
        if node in memo.keys():
            return memo[node]

        successors = gr.get_succ(go,node)
        if successors == [] :
            return 0
        else :
             memo[node] = max(map(max_depth_aux,successors)) + 1
             return memo[node]
    
    return max_depth_aux(node)

def GOTerms(go, gp, all=True, evidence_code=None):
    """
    return the GOTerms associated to the provided gene product (gp)
    
    go: Gene Ontology graph
    gp: gene product
    all: if True, all the GOTerms and their ancestors will be returned, otherwise only the GOTerms directly associated to the gene product will be returned.
    evidence_code: ignored for the moment
    
    Returns a list of GOTerms identifiers, e.g. ['GO:0005215','GO:0005515','GO:0006810','GO:0006974','GO:0008643']
    """
    if all:
        # Equivalent to getting co-accessibles nodes from gp,
        # Excluding gp
        co_accessibles = gr.BFS(go,gp,invert=True)['color']
        res = []
        for node in gr.get_nodes(go):
            if co_accessibles[node] == 'BLACK' and not node == gp:
                res.append(node)
        return res
                
    else :
        # Equivalent to geting the predecessors
        return gr.get_pred(go,gp)

def GeneProducts(go, term, all=True, evidence_code=None):
    """
    return the gene products anotated by the provided GOTerm
    
    go: Gene Ontology graph
    term: GOTerm id
    all: if True, all the gene products directly and undirectly annotated (linked to a descendant of GOTerm) will be return, otherwise only the gene products directly associated to the GOTerm will be returned.
    evidence_code: ignored for the moment

    Returns a list of gene products identifiers, e.g. ['P0AAG5', 'P0AFY6', 'P10907', 'P16676', 'P23886']
    """
    if all:
        # Equivalent to getting accessibles nodes from term,
        # Excluding GOTerms
        accessibles = gr.BFS(go,term)['color']
        res = []
        for node in gr.get_nodes(go):
            if accessibles[node] == 'BLACK' and node[:3] != 'GO:' :
                res.append(node)
        return res
                
    else :
        # Get the successors
        succ = gr.get_succ(go,term)
        # Get their successors
        succ2 = set(flatten(map(lambda x: gr.get_succ(go,x),succ)))
        # Remove the GOTerms on both
        return list(filter(lambda x : x[:3] != 'GO:', succ2.union(succ)))




        
##############
# Main

if __name__ == "__main__":
    
    # Test functions
    
    def flatten(iterable):
        """ Flattens a 2d list """
        return [item for sublist in iterable for item in sublist]
    
    
    def get_roots(go):
        """ Get the roots of go graph """
        res = []
        for node in gr.get_nodes(go):
            if gr.get_pred(go,node) == []:
                res.append(node)
        
        return res
    
    def test_time(n, function, *args, **kargs):
        """ runs function(*args, **kwargs) n times 
        and returns the mean execution time."""
        times = np.zeros(n)
        for i in range(n):
            start = time.time()
            function(*args,**kargs)
            stop = time.time()
            times[i] = stop - start
        
        return np.mean(times)
    
    def test_time_copy(n, function, struct, *args, **kwargs):
        """ runs function(copy.deepcopy(struct), *args, **kwargs) 
        n times and returns the mean execution time.
        Ignores the time needed to copy struct
        """
        times = np.zeros(n)
        for i in range(n):
            struct_copy = copy.deepcopy(struct)
            start = time.time()
            function(struct_copy,*args, **kwargs)
            stop = time.time()
            times[i] = stop - start
        
        return np.mean(times)        
    
    
    # Tests
    gr.newtest()
    print("GeneOntology lib tests")
    
    gr.newtest()
    print("Seulement avec la GO")
    
    gr.newtest()
    # Tests avec seulement la GO
    print("Tests de load_OBO sur go-basic.obo :\n")
    go = load_OBO("go-basic.obo")

    print("Temps moyen d'éxecution :")
    print(test_time(10,load_OBO,"go-basic.obo"))
    
    print("Vérification que la GO est acyclique:")
    print(gr.is_acyclic(go),'\n')
    
    print("Récupération des racines de la GO :")
    roots = get_roots(go)
    print(*roots, sep = ', ')
    root_names = list(map(lambda x: go['nodes'][x]['name'],roots)) 
    print(*root_names, sep = ', ')

    gr.newtest()
    print("Test de max_depth :\n")
    
    print("Profondeur maximale des 3 ontologies :")
    depths = list(map(lambda x: max_depth(go,x),roots))
    print(dict(zip(root_names,depths)))
    print()
    
    def time_max_depth(roots):
        list(map(lambda x: max_depth(go,x),roots))
    
    print("Temps moyen d'éxecution :")
    print(test_time(25,time_max_depth,roots))
    
    gr.newtest()
    print("Caractéristiques diverses :\n")
    
    print("Nombre de sommets dans la GO, i.e. nombre de GO Terms :")
    n_goterms = len(go['nodes'])
    print(n_goterms,'\n')
    
    print("Nombre d'arcs dans la GO :")
    print(go['nb_edges'])
    
    gr.newtest()
    print("Ajout des annotations")
    
    gr.newtest()
    print("Test de load_GOA sur go-basic.obo 119.C_tetani.goa :\n")
    go_copy = copy.deepcopy(go)
    load_GOA(go,"119.C_tetani.goa")

    print("Temps moyen d'éxecution :")
    print(test_time_copy(10,load_GOA,go_copy,"119.C_tetani.goa"))
    
    gr.newtest()
    print("Caractéristiques diverses :\n")
    
    print("Nombre de gene products :")
    print(len(go['nodes']) - n_goterms)
    
    gr.newtest()
    print("Test des fonctions d'association entre gene products et GO Terms:\n")
    
    def get_names(iterator):
        return list(map(lambda x: go['nodes'][x]['name'], iterator))
    
    def get_desc(iterator):
        return list(map(lambda x: go['nodes'][x]['desc'], iterator))
    
    start_prod = 'Q890Z7'
    print("Point de départ pour les tests :")
    print(start_prod)
    print("Un gene product ayant pour description :")
    print(",\n".join(get_desc([start_prod])))
    print()
    
    print("GO terms directement associés : GOTerms, all = False")
    direct_assoc_GOTerms = GOTerms(go,start_prod,all=False)
    pprint(direct_assoc_GOTerms)
    print("Ayant pour noms :")
    print(",\n".join(get_names(direct_assoc_GOTerms)))
    print()
    
    start_go = 'GO:0006221'
    print("Partons donc du GOTerm", start_go,", ayant pour nom :")
    print(",\n".join(get_names([start_go])))
    print()
    
    print("gene products directement associés : GeneProducts, all = False")
    direct_assoc_prod = GeneProducts(go,start_go, all= False)
    pprint(direct_assoc_prod)
    print("Ayant pour description :")
    print(",\n".join(get_desc(direct_assoc_prod)))
    print()
    
    print("Nombre de GO terms associés à ", start_prod," : GOTerms, all = True")
    print(len(GOTerms(go,start_prod,all=True)))
    print()
    
    print("Nombre de gene products associés à ", start_go," : GOTerms, all = True")
    print(len(GeneProducts(go,start_go,all=True)))
    print()
    
    print("Temps moyens d'exécution sur les conditions initiales précédentes :")
    print("GO Terms directs :")
    print(test_time(50,GOTerms,go,start_prod,all=False))
    print("gene products directs :")
    print(test_time(50,GeneProducts,go,start_go,all=False))
    print("GO Terms associés :")
    print(test_time(50,GOTerms,go,start_prod,all=True))
    print("gene products associés :")
    print(test_time(50,GeneProducts,go,start_go,all=True))

    gr.newtest()
    

